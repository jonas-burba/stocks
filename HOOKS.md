# Git hooks

## pre-commit hook to check formatting

```js
#!/usr/bin/env node
const fs = require('fs');
const childProcessExec = require('child_process').exec;
const util = require('util');

const childProcessExecPromisified = util.promisify(childProcessExec);

async function checkFormatting(){
    let formatCheck;
    try {
        formatCheck = await childProcessExecPromisified('"./node_modules/.bin/nx" format:check');
        process.exit(0)
    }
    catch (e) {
        console.log('Format check pre commit hook failed. Check log.');
        console.log(e.stdout);
        process.exit(1);

    }
    finally {
        // console.log(formatCheck && formatCheck.stdout);
        // console.log(formatCheck && formatCheck.stderr);
    }
}
checkFormatting();

// childProcessExec('"./node_modules/.bin/nx" format:check', (error, stdout, stderr) => {
//     if (error) {
//         console.log('error');
//         console.log(stdout);
//         console.log(stderr);
//     } else {
//         console.log('success')
//         console.log(stdout);
//         console.log(stderr);
//     }
// })
```
