import * as http from 'https';
import { environment } from '../../environments/environment';

export const REVERSE_PROXY = 'REVERSE_PROXY';

export const reverseProxyFn = async function(url: string): Promise<any> {
  return new Promise((resolve, reject) => {
    url = `${environment.apiHost}${url}`;
    http
      .get(url, resp => {
        let data = '';
        resp.on('data', chunk => (data += chunk));
        resp.on('end', () => {
          if (resp.statusCode === 200) {
            try {
              resolve(JSON.parse(data));
            } catch (error) {
              reject({ statusCode: 500, statusText: (error as Error).message });
            }
          } else {
            reject({ statusCode: resp.statusCode, statusText: data });
          }
        });
      })
      .on('error', err => {
        reject({ statusCode: 500, statusText: (err as Error).message });
      });
  });
};

export type reverseProxyFnType = typeof reverseProxyFn;
