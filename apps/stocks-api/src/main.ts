/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 **/
import { Server } from 'hapi';
import {
  REVERSE_PROXY,
  reverseProxyFn,
  reverseProxyFnType
} from './app/common/reverseProxy';
import { environment } from './environments/environment';

const init = async () => {
  const { port, host } = environment;
  const server = new Server({ port, host });

  server.method(REVERSE_PROXY, reverseProxyFn, {
    cache: {
      expiresIn: 60 * 60 * 1000,
      staleIn: 60 * 1000,
      staleTimeout: 100,
      generateTimeout: false
    }
  });

  server.route({
    method: 'GET',
    path: '/{p*}',
    handler: (request, h) => {
      const method = server.methods[REVERSE_PROXY];
      const pathAndSearch = `${request.url.pathname}${request.url.search}`;
      return new Promise((resolve, reject) => {
        (method as reverseProxyFnType)(pathAndSearch)
          .then(value => resolve(value))
          .catch(reason =>
            resolve(h.response(reason.statusText).code(reason.statusCode))
          );
      });
    },
    options: {
      cors: true
    }
  });

  server.route({
    method: 'GET',
    path: '/stats',
    handler: (request, h) => {
      return (server.methods[REVERSE_PROXY] as any).cache.stats;
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
