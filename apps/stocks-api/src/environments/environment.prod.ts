export const environment = {
  production: true,
  port: 3332,
  host: 'localhost',
  apiHost: 'https://cloud.iexapis.com'
};
