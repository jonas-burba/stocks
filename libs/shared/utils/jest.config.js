module.exports = {
  name: 'shared-utils',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/shared/utils'
};
