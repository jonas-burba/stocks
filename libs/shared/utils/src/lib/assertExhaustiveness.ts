export function assertExhaustiveness(_: never, message?: string) {
  // tslint:disable-next-line: no-arg
  throw new Error(
    `Should not have been called: ${message || arguments.callee.name}`
  );
}
