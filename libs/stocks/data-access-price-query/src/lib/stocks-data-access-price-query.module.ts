import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { NgModule, Optional, Self, SkipSelf } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { environment } from '@coding-challenge/stocks/data-access-app-config';
import { PriceQueryEffects } from './+state/price-query.effects';
import { PriceQueryFacade } from './+state/price-query.facade';
import {
  priceQueryReducer,
  PRICEQUERY_FEATURE_KEY
} from './+state/price-query.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(PRICEQUERY_FEATURE_KEY, priceQueryReducer),
    EffectsModule.forFeature([PriceQueryEffects])
  ],
  providers: [PriceQueryFacade]
})
export class StocksDataAccessPriceQueryModule {
  constructor(
    @Optional() @Self() httpClientFromSelf: HttpClient,
    @Optional() @SkipSelf() httpClientFromParent: HttpClient
  ) {
    if (!environment.production && httpClientFromSelf) {
      console.warn(
        `${this.constructor.name}: Consider providing HttpClient at root.`
      );
    }
  }
}
