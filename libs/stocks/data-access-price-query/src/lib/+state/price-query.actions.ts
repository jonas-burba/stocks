import { Action } from '@ngrx/store';
import { PriceQueryResponse } from './price-query.type';

export enum PriceQueryActionTypes {
  SelectSymbol = 'priceQuery.selectSymbol',
  FetchPriceQueryByNamedRange = 'priceQuery.fetchByNamedRange',
  FetchPriceQueryByCustomRange = 'priceQuery.fetchByCustomRange',
  PriceQueryFetched = 'priceQuery.fetched',
  PriceQueryFetchError = 'priceQuery.error'
}

export class FetchPriceQueryByNamedRange implements Action {
  readonly type = PriceQueryActionTypes.FetchPriceQueryByNamedRange;
  constructor(public symbol: string, public period: string) {}
}

export class FetchPriceQueryByCustomRange implements Action {
  readonly type = PriceQueryActionTypes.FetchPriceQueryByCustomRange;
  constructor(public symbol: string, public from: Date, public to: Date) {}
}

export class PriceQueryFetchError implements Action {
  readonly type = PriceQueryActionTypes.PriceQueryFetchError;
  constructor(public error: any) {}
}

export class PriceQueryFetched implements Action {
  readonly type = PriceQueryActionTypes.PriceQueryFetched;
  constructor(public queryResults: PriceQueryResponse[]) {}
}

export class SelectSymbol implements Action {
  readonly type = PriceQueryActionTypes.SelectSymbol;
  constructor(public symbol: string) {}
}

export type PriceQueryAction =
  | FetchPriceQueryByNamedRange
  | FetchPriceQueryByCustomRange
  | PriceQueryFetched
  | PriceQueryFetchError
  | SelectSymbol;

export const fromPriceQueryActions = {
  FetchPriceQueryByNamedRange,
  FetchPriceQueryByCustomRange,
  PriceQueryFetched,
  PriceQueryFetchError,
  SelectSymbol
};
