import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import {
  FetchPriceQueryByNamedRange,
  FetchPriceQueryByCustomRange
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { getSelectedSymbol, getAllPriceQueries } from './price-query.selectors';
import { map, skip } from 'rxjs/operators';

@Injectable()
export class PriceQueryFacade {
  selectedSymbol$ = this.store.pipe(select(getSelectedSymbol));
  priceQueries$ = this.store.pipe(
    select(getAllPriceQueries),
    skip(1),
    map(priceQueries =>
      priceQueries.map(priceQuery => [priceQuery.date, priceQuery.close])
    )
  );

  constructor(private store: Store<PriceQueryPartialState>) {}

  fetchQuote(symbol: string, period: string) {
    this.store.dispatch(new FetchPriceQueryByNamedRange(symbol, period));
  }

  fetchQuoteByCustomRange(
    symbol: string,
    from: Date | undefined,
    to: Date | undefined
  ) {
    this.store.dispatch(
      new FetchPriceQueryByCustomRange(
        symbol,
        from || new Date(0),
        to || new Date()
      )
    );
  }
}
