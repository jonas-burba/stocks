import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import { environment } from '@coding-challenge/stocks/data-access-app-config';
import {
  FetchPriceQueryByNamedRange,
  FetchPriceQueryByCustomRange,
  PriceQueryActionTypes,
  PriceQueryFetched,
  PriceQueryFetchError
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQueryByNamedRange$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQueryByNamedRange,
    {
      id: (a, state) =>
        'use switchMap (i.e. cancellable ) instead of concatMap',
      run: (
        action: FetchPriceQueryByNamedRange,
        state: PriceQueryPartialState
      ) => {
        return this.httpClient
          .get<PriceQueryResponse[]>(
            `${environment.apiURL}/api/stock/${action.symbol}/chart/${
              action.period
            }?token=${environment.apiKey}`
          )
          .pipe(map(resp => new PriceQueryFetched(resp)));
      },

      onError: (action: FetchPriceQueryByNamedRange, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  @Effect() loadPriceQueryByCustomRange$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQueryByCustomRange,
    {
      id: (a, state) =>
        'use switchMap (i.e. cancellable ) instead of concatMap',
      run: (
        action: FetchPriceQueryByCustomRange,
        state: PriceQueryPartialState
      ) => {
        return this.httpClient
          .get<PriceQueryResponse[]>(
            `${environment.apiURL}/api/stock/${
              action.symbol
            }/chart/max?token=${environment.apiKey}`
          )
          .pipe(
            // client-side filtering as close as possible to origin/backend
            // because backend should should handle filtering
            map(resp =>
              resp.filter(v => {
                const date = new Date(v.date);
                return action.from <= date && date <= action.to;
              })
            ),
            map(resp => new PriceQueryFetched(resp))
          );
      },

      onError: (action: FetchPriceQueryByCustomRange, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  constructor(
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {}
}
