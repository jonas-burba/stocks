export interface IFormValue {
  symbol: SymbolQuery | string | undefined;
  period: string;
}

export interface SymbolQuery {
  symbol: string;
  name: string;
}

export type Range = 'max' | '5y' | '2y' | '1y' | 'ytd' | '6m' | '3m' | '1m';
