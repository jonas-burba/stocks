import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  OnDestroy,
  TrackByFunction
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable, concat, Subscription } from 'rxjs';
import {
  map,
  filter,
  tap,
  switchMap,
  take,
  share,
  distinctUntilChanged
} from 'rxjs/operators';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { SymbolQueryFacade } from '@coding-challenge/stocks/data-access-symbol-query';
import { assertExhaustiveness } from '@coding-challenge/shared/utils';
import { MatSelectChange } from '@angular/material';
import { startOfYear, addMonths, addYears } from 'date-fns';
import { isEqual } from 'lodash-es';
import { SymbolQuery, IFormValue, Range } from './stocks.types';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StocksComponent implements OnInit, OnDestroy {
  today = new Date();
  stockPickerForm: FormGroup;

  private availableSymbols$ = this.symbolQuery.allSymbol$;
  filteredOptions$: Observable<SymbolQuery[]>;

  quotes$ = this.priceQuery.priceQueries$;

  timePeriods: { viewValue: string; value: Range }[] = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];

  private valueChanges$: Observable<IFormValue>;
  private subscription: Subscription;

  constructor(
    fb: FormBuilder,
    private priceQuery: PriceQueryFacade,
    private symbolQuery: SymbolQueryFacade
  ) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      from: [null],
      to: [null]
    });
  }

  ngOnInit() {
    this.symbolQuery.loadAll();

    this.valueChanges$ = this.stockPickerForm.valueChanges;

    this.filteredOptions$ = concat(
      this.availableSymbols$.pipe(take(2)),
      this.valueChanges$.pipe(
        map(v => v.symbol),
        filter((v): v is string | SymbolQuery => v !== null && v !== undefined),
        switchMap(v =>
          this.availableSymbols$.pipe(
            map(availableSymbols => {
              const criteria = (typeof v === 'string'
                ? v
                : v.name
              ).toLowerCase();
              return availableSymbols.filter(({ name }) =>
                name.toLowerCase().startsWith(criteria)
              );
            })
          )
        )
      )
    ).pipe(
      map(v => v.slice(0, 9)),
      share()
    );

    this.subscription = this.valueChanges$
      .pipe(distinctUntilChanged((a, b) => isEqual(a, b)))
      .subscribe(this.fetchQuote.bind(this));
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  selectDatesFromNamedRange($event: MatSelectChange): void {
    const value = $event.value as Range;
    const [from, to] = this.timePeriodsNamedRangeToDateRange(value);
    this.stockPickerForm.patchValue({ from, to });
  }

  private fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, period } = this.stockPickerForm.value as IFormValue;
      if (typeof symbol === 'object') {
        this.priceQuery.fetchQuoteByCustomRange(
          symbol.symbol,
          this.stockPickerForm.value.from,
          this.stockPickerForm.value.to
        );
      }
    }
  }

  private timePeriodsNamedRangeToDateRange(
    value: Range
  ): [Date | undefined, Date | undefined] {
    const now = new Date();
    let start: Date | undefined;
    switch (value) {
      case '1m':
        start = addMonths(now, -1);
        break;
      case '3m':
        start = addMonths(now, -3);
        break;
      case '6m':
        start = addMonths(now, -6);
        break;
      case 'ytd':
        start = startOfYear(now);
        break;
      case '1y':
        start = addYears(now, -1);
        break;
      case '2y':
        start = addYears(now, -2);
        break;
      case '5y':
        start = addYears(now, -5);
        break;
      case 'max':
        break;
      default:
        assertExhaustiveness(value);
        break;
    }
    return [start, start ? now : undefined];
  }

  displayFn(symbol: SymbolQuery | undefined): string | undefined {
    return symbol ? symbol.name : undefined;
  }

  trackByFn: TrackByFunction<SymbolQuery> = (index, item) => item.symbol;
}
