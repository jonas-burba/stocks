module.exports = {
  name: 'stocks-data-access-symbol-query',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/stocks/data-access-symbol-query',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
