import { SymbolQueryResponse, SymbolQuery } from './symbol-query.type';

export function transformSymbolQueryResponse(
  response: SymbolQueryResponse[]
): SymbolQuery[] {
  return response.map(({ symbol, name }) => ({ symbol, name }));
}
