import {
  SymbolQueryAction,
  SymbolQueryActionTypes
} from './symbol-query.actions';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { SymbolQuery } from './symbol-query.type';

export const SYMBOLQUERY_FEATURE_KEY = 'symbolQuery';

export interface SymbolQueryState extends EntityState<SymbolQuery> {}

export const symbolQueryAdapter: EntityAdapter<
  SymbolQuery
> = createEntityAdapter<SymbolQuery>({
  selectId: symbolQuery => symbolQuery.symbol,
  sortComparer: (a, b) => a.symbol.localeCompare(b.symbol)
});

export interface SymbolQueryPartialState {
  readonly [SYMBOLQUERY_FEATURE_KEY]: SymbolQueryState;
}

export const initialState: SymbolQueryState = symbolQueryAdapter.getInitialState();

export function symbolQueryReducer(
  state: SymbolQueryState = initialState,
  action: SymbolQueryAction
): SymbolQueryState {
  switch (action.type) {
    case SymbolQueryActionTypes.SymbolQueryFetched: {
      return symbolQueryAdapter.addAll(action.queryResults, state);
    }
  }
  return state;
}
