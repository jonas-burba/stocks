export type SymbolQuery = {
  symbol: string;
  name: string;
};

export type SymbolQueryResponse = {
  symbol: string;
  name: string;
  date: string;
  type: string;
  iexId: string;
  region: string;
  currency: string;
  isEnabled: string;
};
