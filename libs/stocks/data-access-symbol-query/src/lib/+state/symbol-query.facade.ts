import { Injectable } from '@angular/core';

import { select, Store } from '@ngrx/store';

import { SymbolQueryPartialState } from './symbol-query.reducer';
import { symbolQuery } from './symbol-query.selectors';
import { FetchSymbolQuery } from './symbol-query.actions';

@Injectable()
export class SymbolQueryFacade {
  allSymbol$ = this.store.pipe(select(symbolQuery.getAllSymbolQueries));

  constructor(private store: Store<SymbolQueryPartialState>) {}

  loadAll() {
    this.store.dispatch(new FetchSymbolQuery());
  }
}
