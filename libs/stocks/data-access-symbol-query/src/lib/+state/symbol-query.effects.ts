import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map, tap } from 'rxjs/operators';
import { environment } from '@coding-challenge/stocks/data-access-app-config';

import { SymbolQueryPartialState } from './symbol-query.reducer';
import {
  FetchSymbolQuery,
  SymbolQueryFetched,
  SymbolQueryFetchError,
  SymbolQueryActionTypes
} from './symbol-query.actions';
import { SymbolQueryResponse } from './symbol-query.type';

@Injectable()
export class SymbolQueryEffects {
  @Effect() loadSymbolQuery$ = this.dataPersistence.fetch(
    SymbolQueryActionTypes.FetchSymbolQuery,
    {
      id: (a, state) =>
        'use switchMap (i.e. cancellable ) instead of concatMap',
      run: (action: FetchSymbolQuery, state: SymbolQueryPartialState) => {
        return this.httpClient
          .get<SymbolQueryResponse[]>(
            `${environment.apiURL}/api/ref-data/symbols?token=${
              environment.apiKey
            }`
          )
          .pipe(map(resp => new SymbolQueryFetched(resp)));
      },

      onError: (action: FetchSymbolQuery, error) => {
        return new SymbolQueryFetchError(error);
      }
    }
  );

  @Effect({ dispatch: false }) errorSymbolQuery$ = this.actions$.pipe(
    ofType(SymbolQueryActionTypes.SymbolQueryFetchError),
    tap(v => !environment.production && console.error('Error', v)) // tslint:disable-line: no-unused-expression),
  );

  constructor(
    private actions$: Actions,
    private httpClient: HttpClient,
    private dataPersistence: DataPersistence<SymbolQueryPartialState>
  ) {}
}
