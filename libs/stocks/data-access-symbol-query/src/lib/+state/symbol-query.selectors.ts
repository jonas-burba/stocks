import { createFeatureSelector, createSelector } from '@ngrx/store';
import {
  SYMBOLQUERY_FEATURE_KEY,
  SymbolQueryState,
  symbolQueryAdapter
} from './symbol-query.reducer';

const getSymbolQueryState = createFeatureSelector<SymbolQueryState>(
  SYMBOLQUERY_FEATURE_KEY
);

const { selectAll } = symbolQueryAdapter.getSelectors();

const getAllSymbolQueries = createSelector(
  getSymbolQueryState,
  selectAll
);

export const symbolQuery = {
  getAllSymbolQueries
};
