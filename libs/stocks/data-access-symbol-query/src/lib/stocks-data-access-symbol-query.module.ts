import { NgModule, Optional, Self, SkipSelf } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { environment } from '@coding-challenge/stocks/data-access-app-config';
import {
  SYMBOLQUERY_FEATURE_KEY,
  initialState as symbolInitialState,
  symbolQueryReducer
} from './+state/symbol-query.reducer';
import { SymbolQueryEffects } from './+state/symbol-query.effects';
import { SymbolQueryFacade } from './+state/symbol-query.facade';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(SYMBOLQUERY_FEATURE_KEY, symbolQueryReducer, {
      initialState: symbolInitialState
    }),
    EffectsModule.forFeature([SymbolQueryEffects])
  ],
  providers: [SymbolQueryFacade]
})
export class StocksDataAccessSymbolQueryModule {
  constructor(
    @Optional() @Self() httpClientFromSelf: HttpClient,
    @Optional() @SkipSelf() httpClientFromParent: HttpClient
  ) {
    if (!environment.production && httpClientFromSelf) {
      console.warn(
        `${this.constructor.name}: Consider providing HttpClient at root.`
      );
    }
  }
}
