import { async, TestBed } from '@angular/core/testing';
import { StocksDataAccessSymbolQueryModule } from './stocks-data-access-symbol-query.module';

describe('StocksDataAccessSymbolQueryModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StocksDataAccessSymbolQueryModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(StocksDataAccessSymbolQueryModule).toBeDefined();
  });
});
