# TODO:

 - [ ] refactor StocksComponent to sub-components to better align with single responsibility principle
 - [ ] do error handling, e.g. snackbar
 - [ ] write tests
 - [x] [make git hook to check formatting before committing](hooks.md)


# App

## How to run the application

There are two apps: `stocks` and `stocks-api`.

- `stocks` is the front-end. It uses Angular 7, Material, Ngrx, Nx. You can run this using `yarn serve:stocks`
- `stocks-api` uses Hapi and has a very minimal implementation. You can start the API server with `yarn serve:stocks-api`

A proxy has been set up in `stocks` to proxy calls to `locahost:3332` which is the port that the Hapi server listens on.

> You need to register for a token here: https://iexcloud.io/cloud-login#/register/ Use this token in the `environment.ts` file for the `data-access-app-config` lib.

